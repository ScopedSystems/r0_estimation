
% Author: Cyrus Minwalla
% Date: June 8th, 2012


function plot_exp_fit(range_crop, SNR, log_x, log_y, pfit, vis_range, cur_cam, cur_event, label_x, label_y, xlims)

    
    namestring = sprintf('SNR vs Range: 2012Feb28 Event%0.1d Cam%0.1d', cur_event, cur_cam);
    %fitstring = sprintf('Fit: \\gamma=%0.3f, x0=%4.2f, \\nu=%2.2f', pfit(1), pfit(3), pfit(5));
    fitstring = sprintf('Fit (ae^{bx}): b=%0.4f, a=%4.2f', pfit(1), pfit(2));
    rose_crit = 5*ones(1, numel(range_crop));
    %plot(range_crop, rose_crit);
    %hold on
    plot_min = min(SNR(:)-3);
    plot_max = 120;%max(SNR(:)+5);
    db_scale = [plot_min:plot_max];
    fixed_range = (vis_range)*ones(1,numel(db_scale));
    vis_upper = fixed_range+(920*1.852)/1000;
    vis_lower = fixed_range-(920*1.852)/1000;
    %plot(fixed_range, db_scale);

    % Plot fit with data.
    figure( 'Name', namestring );
    h1 = plot(range_crop, SNR, '.b', log_x, log_y, '-r');
    hold on;
    %h2 = plot(range_crop, rose_crit, '--r', fixed_range, db_scale, '-g', vis_upper, db_scale, '--g', vis_lower, db_scale, '--g');
    h2 = plot(range_crop, rose_crit, '--r');
    hh = [h1(1), h1(2), h2(1)]; %, h2(2)];
    %set(h1(2), 'Color',[1 0 0], 'LineWidth', 2);
    %set(h1(3), 'Color',[0 0 0]);
    %set(h1(4), 'Color',[0 0 0]);
    set(h2(1), 'Color', [1 0 0], 'LineWidth', 2);
    %set(h2(2), 'Color', [0 0.75 0], 'LineWidth', 2);
    %set(h2(3), 'Color', [0 0.75 0]);
    %set(h2(4), 'Color', [0 0.75 0]);
    set(gca, 'FontSize',12);
    set(gca, 'XLim', xlims);
    set(gca, 'YLim', [plot_min plot_max]);
    legend(hh, 'Ground-truth SNR', fitstring, 'Threshold','Pilot Detection', 'Pilot Error',...
        'Location', 'NorthEast', 'FontSize', '12', 'Interpreter', 'Latex');
    
    % Label axes
    xlabel(label_x );
    ylabel( label_y );
    grid on
end
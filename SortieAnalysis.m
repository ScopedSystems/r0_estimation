

% Author: Cyrus Minwalla
% Date: June 25 2020
% Organization: Scoped Systems
% Last modified: 2020-Nov-29

clear all;
close all;

%load('rangeCorrected_to_cyrusmat_ROI_96x60.mat');
filename = '200by200_tocyrus.mat';
load(filename);
thresh = 3.99;
R_systematic = 0.02;
R0_all = []
R0error_all=[];
numSorties = 10;

%Range is 14, CNR is 8, signal is 9, background is 10, noise is 11
for i=1:8%1:numSorties
    sortie = to_cyrusV2(i).sortieValues;
    sortieRangekm = sortie(:,14)/1000;
    sortieSignal = sortie(:,9);
    sortieBack = sortie(:,10);
    sortieNoise = sortie(:,11);
    sortieSignalBack = abs(sortieSignal - sortieBack);
    sortieCNR = abs(sortieSignal - sortieBack)./sortieNoise;
    if(i==3)
        sortieCNR = abs(sortieSignal - 201.59)./sortieNoise; 
        sortieCNR = sortieCNR(1:189);
        sortieRangekm = sortieRangekm(1:189);
        
    end
    if(i==4)
        sortieCNR = sortieCNR(1:249);
        sortieRangekm = sortieRangekm(1:249); 
    end
    if(i==6)
        sortieCNR = abs(sortieSignal - 142.34)./sortieNoise; 
    end
    if(i==7)
        sortieCNR = abs(sortieSignal - 201.59)./sortieNoise; 
    end
    if(i==8)
        sortieCNR = abs(sortieSignal - 201.59)./sortieNoise; 
    end
    x = [1:numel(sortieRangekm)]'
    fit = polyfit(x, sortieRangekm,1)
    sortieRangekmlinear = fit(1)*x + fit(2);
    [pfit,resnorm,residual, ci, delta_p, fitx, fity] = exp_fit(sortieCNR, sortieRangekmlinear);
    f=figure;plot(sortieRangekmlinear, sortieCNR, '.'); hold on; plot(fitx, fity);
    title(['Sortie ' num2str(i) ]);
    xlabel('Range [km]');
    ylabel('CNR [dn]');
    fitstring = sprintf('Fit (ae^{bx}): \n a=%4.2f, \\sigma_a=%4.2f \n b=%0.4f, \\sigma_b=%0.4f', pfit(1), delta_p(1), pfit(2), delta_p(2));
    leg = legend('Raw values', fitstring, 'Interpreter', 'tex');
    saveString = ['CNR_Sortie_' num2str(i) '_' filename '.fig' ];
    saveas(f, saveString);
    b = -1*pfit(1); a = pfit(2);
    d_b = delta_p(1); d_a = delta_p(2);
    R0_all(i) = 1/b *(log(a/thresh));
    R0error_all(i) = sqrt( (d_a / (a*b))^2 + ((1/b^2)*log(thresh/a)*d_b)^2) + R_systematic; 
end
% sortie1 = to_cyrusV2(1).sortieValues;
% sortie1Rangekm = sortie1(:,14)/1000;
% sortie1Signal = sortie1(:,9);
% sortie1Back = sortie1(:,10);
% sortie1Noise = sortie1(:,11);
% sortie1SignalBack = abs(sortie1Signal - sortie1Back);
% sortie1CNR = abs(sortie1Signal - sortie1Back)./sortie1Noise;
% %sortie2CNR = sortie2CNR(1:189);
% %sortie2Rangekm = sortie2Rangekm(1:189);
% x = [1:numel(sortie1Rangekm)];
% fit1 = polyfit(x', sortie1Rangekm,1)
% sortie1Rangekmlinear = fit1(1)*x' + fit1(2);
% [pfit,resnorm,residual, ci, delta_p, fitx, fity] = exp_fit(sortie1CNR, sortie1Rangekmlinear);
% figure;plot(sortie1Rangekmlinear, sortie1CNR, '.'); hold on; plot(fitx, fity);
% title('Sortie 1');
% xlabel('Range [km]');
% ylabel('CNR [dn]');
% fitstring = sprintf('Fit ($ae^{bx}$): b=%0.4f, a=%4.2f', pfit(1), pfit(2));
% leg = legend('Raw values', fitstring);
% set(leg, 'Interpreter', 'latex');
% R0_all(1) = 1/pfit(1) *(log(pfit(2)/thresh));
% d_b = delta_p(1); d_a = delta_p(2);
% R0error_all(1) = sqrt( (d_a / (a*b))^2 + ((1/b^2)*log(thresh/a)*d_b)^2) + R_systematic 
% 
% 
% 
% sortie2 = to_cyrusV2(2).sortieValues;
% sortie2Rangekm = sortie2(:,14)/1000;
% sortie2Signal = sortie2(:,9);
% sortie2Back = sortie2(:,10);
% sortie2Noise = sortie2(:,11);
% sortie2SignalBack = abs(sortie2Signal - sortie2Back);
% sortie2CNR = abs(sortie2Signal - sortie2Back)./sortie2Noise;
% %sortie2CNR = sortie2CNR(1:189);
% %sortie2Rangekm = sortie2Rangekm(1:189);
% x = [1:numel(sortie2Rangekm)];
% fit2 = polyfit(x', sortie2Rangekm,1)
% sortie2Rangekmlinear = fit2(1)*x' + fit2(2);
% [pfit,resnorm,residual, ci, delta_p, fitx, fity] = exp_fit(sortie2CNR, sortie2Rangekmlinear);
% figure;plot(sortie2Rangekmlinear, sortie2CNR, '.'); hold on; plot(fitx, fity);
% title('Sortie 2');
% xlabel('Range [km]');
% ylabel('CNR [dn]');
% fitstring = sprintf('Fit ($ae^{bx}$): b=%0.4f, a=%4.2f', pfit(1), pfit(2));
% leg = legend('Raw values', fitstring);
% set(leg, 'Interpreter', 'latex');
% 
% 
% sortie3 = to_cyrusV2(3).sortieValues;
% sortie3Rangekm = sortie3(:,14)/1000;
% 
% sortie3Signal = sortie3(:,9);
% sortie3Back = sortie3(:,10);
% sortie3Noise = sortie3(:,11);
% 
% sortie3SignalBack = abs(sortie3Signal - sortie3Back);
% sortie3CNR = abs(sortie3Signal - sortie3Back)./sortie3Noise;
% 
% sortie3CNR = sortie3CNR(1:189);
% sortie3Rangekm = sortie3Rangekm(1:189);
% x = [1:numel(sortie3Rangekm)];
% fit3 = polyfit(x', sortie3Rangekm,1)
% sortie3Rangekmlinear = fit3(1)*x' + fit3(2);
% 
% [pfit,resnorm,residual, ci, delta_p, fitx, fity] = exp_fit(sortie3CNR, sortie3Rangekmlinear);
% figure;plot(sortie3Rangekmlinear, sortie3CNR, '.'); hold on; plot(fitx, fity);
% title('Sortie 3');
% xlabel('Range [km]');
% ylabel('CNR [dn]');
% fitstring = sprintf('Fit ($ae^{bx}$): b=%0.4f, a=%4.2f', pfit(1), pfit(2));
% 
% leg = legend('Raw values', fitstring);
% set(leg, 'Interpreter', 'latex');
% 
% sortie4 = to_cyrusV2(4).sortieValues;
% 
% sortie4Rangekm = sortie4(:,14)/1000;
% 
% sortie4Signal = sortie4(:,9);
% sortie4Back = sortie4(:,10);
% sortie4Noise = sortie4(:,11);
% 
% sortie4SignalBack = abs(sortie4Signal - sortie4Back);
% sortie4CNR = abs(sortie4Signal - sortie4Back)./sortie4Noise;
% 
% sortie4CNR = sortie4CNR(1:249);
% sortie4Rangekm = sortie4Rangekm(1:249);
% x = [1:numel(sortie4Rangekm)]
% fit4 = polyfit(x', sortie4Rangekm,1)
% sortie4Rangekmlinear = fit4(1)*x + fit4(2);
% 
% 
% [pfit,resnorm,residual, ci, delta_p, fitx, fity] = exp_fit(sortie4CNR, sortie4Rangekmlinear);
% figure;plot(sortie4Rangekmlinear, sortie4CNR, '.'); hold on; plot(fitx, fity);
% title('Sortie 4');
% xlabel('Range [km]');
% ylabel('CNR [dn]');
% fitstring = sprintf('Fit ($ae^{bx}$): b=%0.4f, a=%4.2f', pfit(1), pfit(2));
% 
% leg = legend('Raw values', fitstring);
% set(leg, 'Interpreter', 'latex');
% 
% sortie6 = to_cyrusV2(6).sortieValues;
% 
% sortie6Rangekm = sortie6(:,14)/1000;
% 
% sortie6Signal = sortie6(:,9);
% sortie6Back = sortie6(:,10);
% sortie6Noise = sortie6(:,11);
% 
% sortie6SignalBack = abs(sortie6Signal - sortie6Back);
% sortie6CNR = abs(sortie6Signal - sortie6Back)./sortie6Noise;
% 
% % sortie6CNR = sortie6CNR(1:249);
% % sortie6Rangekm = sortie6Rangekm(1:249);
% x = [1:numel(sortie6Rangekm)]
% fit6 = polyfit(x', sortie6Rangekm,1)
% sortie6Rangekmlinear = fit6(1)*x + fit6(2);
% [pfit,resnorm,residual, ci, delta_p, fitx, fity] = exp_fit(sortie6CNR, sortie6Rangekmlinear);
% figure;plot(sortie6Rangekmlinear, sortie6CNR, '.'); hold on; plot(fitx, fity);
% title('Sortie 6');
% xlabel('Range [km]');
% ylabel('CNR [dn]');
% fitstring = sprintf('Fit ($ae^{bx}$): b=%0.4f, a=%4.2f', pfit(1), pfit(2));
% leg = legend('Raw values', fitstring);
% set(leg, 'Interpreter', 'latex');
% 
% 
% 
% sortie7 = to_cyrusV2(7).sortieValues;
% sortie7Rangekm = sortie7(:,14)/1000;
% sortie7Signal = sortie7(:,9);
% sortie7Back = sortie7(:,10);
% sortie7Noise = sortie7(:,11);
% sortie7SignalBack = abs(sortie7Signal - sortie7Back);
% sortie7CNR = abs(sortie7Signal - sortie7Back)./sortie7Noise;
% %sortie7CNR = sortie7CNR(1:249);
% %sortie7Rangekm = sortie7Rangekm(1:249);
% x = [1:numel(sortie7Rangekm)]
% fit = polyfit(x', sortie7Rangekm,1)
% sortie7Rangekmlinear = fit(1)*x + fit(2);
% [pfit,resnorm,residual, ci, delta_p, fitx, fity] = exp_fit(sortie7CNR, sortie7Rangekmlinear);
% figure;plot(sortie7Rangekmlinear, sortie7CNR, '.'); hold on; plot(fitx, fity);
% title('Sortie 7');
% xlabel('Range [km]');
% ylabel('CNR [dn]');
% fitstring = sprintf('Fit ($ae^{bx}$): b=%0.4f, a=%4.2f', pfit(1), pfit(2));
% leg = legend('Raw values', fitstring);
% set(leg, 'Interpreter', 'latex');
% 
% sortie8 = to_cyrusV2(8).sortieValues;
% sortieRangekm = sortie8(:,14)/1000;
% sortieSignal = sortie8(:,9);
% sortieBack = sortie8(:,10);
% sortieNoise = sortie8(:,11);
% sortieSignalBack = abs(sortieSignal - sortieBack);
% sortieCNR = abs(sortieSignal - sortieBack)./sortieNoise;
% %sortieCNR = sortieCNR(1:249);
% %sortieRangekm = sortieRangekm(1:249);
% x = [1:numel(sortieRangekm)]
% fit = polyfit(x', sortieRangekm,1)
% sortieRangekmlinear = fit(1)*x + fit(2);
% [pfit,resnorm,residual, ci, delta_p, fitx, fity] = exp_fit(sortieCNR, sortieRangekmlinear);
% figure;plot(sortieRangekmlinear, sortieCNR, '.'); hold on; plot(fitx, fity);
% title('Sortie 8');
% xlabel('Range [km]');
% ylabel('CNR [dn]');
% fitstring = sprintf('Fit ($ae^{bx}$): b=%0.4f, a=%4.2f', pfit(1), pfit(2));
% leg = legend('Raw values', fitstring);
% set(leg, 'Interpreter', 'latex');
% 
% 
% sortie9 = to_cyrusV2(9).sortieValues;
% sortieRangekm = sortie9(:,14)/1000;
% sortieSignal = sortie9(:,9);
% sortieBack = sortie9(:,10);
% sortieNoise = sortie9(:,11);
% sortieSignalBack = abs(sortieSignal - sortieBack);
% sortieCNR = abs(sortieSignal - sortieBack)./sortieNoise;
% %sortieCNR = sortieCNR(1:249);
% %sortieRangekm = sortieRangekm(1:249);
% x = [1:numel(sortieRangekm)]
% fit = polyfit(x', sortieRangekm,1)
% sortieRangekmlinear = fit(1)*x + fit(2);
% [pfit,resnorm,residual, ci, delta_p, fitx, fity] = exp_fit(sortieCNR, sortieRangekmlinear);
% figure;plot(sortieRangekmlinear, sortieCNR, '.'); hold on; plot(fitx, fity);
% title('Sortie 9');
% xlabel('Range [km]');
% ylabel('CNR [dn]');
% fitstring = sprintf('Fit ($ae^{bx}$): b=%0.4f, a=%4.2f', pfit(1), pfit(2));
% leg = legend('Raw values', fitstring);
% set(leg, 'Interpreter', 'latex');
% 
% 
% 
% sortie10 = to_cyrusV2(10).sortieValues;
% sortieRangekm = sortie10(:,14)/1000;
% sortieSignal = sortie10(:,9);
% sortieBack = sortie10(:,10);
% sortieNoise = sortie10(:,11);
% sortieSignalBack = abs(sortieSignal - sortieBack);
% sortieCNR = abs(sortieSignal - sortieBack)./sortieNoise;
% %sortieCNR = sortieCNR(1:249);
% %sortieRangekm = sortieRangekm(1:249);
% x = [1:numel(sortieRangekm)]
% fit = polyfit(x', sortieRangekm,1)
% sortieRangekmlinear = fit(1)*x + fit(2);
% [pfit,resnorm,residual, ci, delta_p, fitx, fity] = exp_fit(sortieCNR, sortieRangekmlinear);
% figure;plot(sortieRangekmlinear, sortieCNR, '.'); hold on; plot(fitx, fity);
% title('Sortie 10');
% xlabel('Range [km]');
% ylabel('CNR [dn]');
% fitstring = sprintf('Fit ($ae^{bx}$): b=%0.4f, a=%4.2f', pfit(1), pfit(2));
% leg = legend('Raw values', fitstring);
% set(leg, 'Interpreter', 'latex');
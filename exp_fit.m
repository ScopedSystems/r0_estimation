

% Author: Cyrus Minwalla
% Date: June 4th 2012
% 
% Purpose: Logistic fit function
%

function [pfit,resnorm,residual, ci, delta_p, fitx, fity] = exp_fit( ydata, xdata)

nval=1000;
%Max = max(ydata);

% Initial guesses for parameters
g = 0;
a = 155;
%Lower bounds
lb=[-1 -400];
%Upper bounds
ub=[1 400];
% Parameter Array
p = [g a];

% Modify Settings in Optimization Toolbox
OPTIONS=optimset('lsqcurvefit');
OPTIONS.DiffMinChange=1.0e-10;
OPTIONS.DiffMaxChange=1.0;
OPTIONS.TolX=1e-11;
OPTIONS.TolFun=1e-12;
OPTIONS.MaxFunEvals=1e4;
OPTIONS.MaxIter=1e4;

% Perform fitting w/ OPTIONS. 
% Note: Using an anonymous function to pass fixed value for max

[pfit, resnorm, residual, exit, output, lambda, jacob] = lsqcurvefit(@exponent, p, xdata, ydata,[], [], OPTIONS);
%[pfit, resnorm, residual, exit, output, lambda, jacob] = lsqcurvefit(@(p,xdata) logistic(p,xdata,Max), ...
%    p, xdata, ydata, lb,ub,OPTIONS);

% Print Results
disp(['Initial Guess  (gamma, a): ' num2str(p)]);
disp(['Fit Parameters (gamma, a): ' num2str(pfit)]);

% Compute Confidence Intervals on Parameters * Requires Statistical Analysis Toolbox
if( nargout > 3 )
    ci=nlparci(pfit,residual,jacob);
    delta_p=ci(:,2)-pfit';
    delta_p = delta_p / 2;
    
    % Display 1-sigma Confidence Intervals w/ Unshifted x0 value
    fprintf('\r gamma= %0.9f +/- %0.9f \r a= %0.5f +/- %0.5f\r',pfit(1),delta_p(1),pfit(2),delta_p(2));
end

step = (max(xdata) - min(xdata))/nval;
fitx =[min(xdata):step:max(xdata)-step];
fity = exponent(pfit, fitx);
end

function f_x = exponent(p, x);

g = p(1);   
a =p(2); 
f_x = a * exp(g*x);

end